#!/usr/bin/env python
# coding: utf-8

import keras
import pandas as pd
from keras.layers import Dense, Flatten, Dropout
from keras.layers.convolutional import Conv2D
from keras.models import Sequential
from .utils import *
import os

batch_size = 1024
num_classes = 3
epochs = 10000



# os.environ['CUDA_VISIBLE_DEVICES'] = '-1'

print(os.getcwd())
# storage_name = "gs://gold-keras/"
# from .utils import *
storage_name = "datasets/"

pd_data = pd.read_csv("%sEURUSD-5m-2015-2019.csv" % storage_name)
# pd_data = pd.read_csv("../deep-learning/EURUSD-5m-2015-2019.csv")
pd_data = pd_data.drop(columns=['Timestamp'])

print(pd_data)
pd_data = pd_data[['High', 'Open', 'Close', 'Low', 'Volume', 'Date']]


params = 4
pips_diff = 0.0001 * 40
print(pips_diff)
n_ahead = 100
sequence_length = 2000

data = pd_data[['High', 'Open', 'Close', 'Low']].values

print(data.shape)

sequence_number = data.shape[0] - sequence_length + 1
print(sequence_number)

def get_x_data(data):
    x_data = np.empty(shape=(sequence_number, sequence_length, params))

    for x in range(sequence_length, data.shape[0] + 1):
        x_data[x - sequence_length] = data[x - sequence_length:x]
    return x_data[:-1]


def normalize_data_per_batch(x_data):
    # print(  (x_data-x_data.min())/(x_data.max()-x_data.min))
    for i in range(0, x_data.shape[0]):
        #         print(x_data[i].shape)
        x_data[i] = normalize_data(x_data[i])
    return x_data


x_data = get_x_data(data)
y_data = np.asarray(get_y_data(x_data, n_ahead, sequence_length, pips_diff))
x_data=normalize_data_per_batch(x_data)

print(y_data)
unique, counts = np.unique(y_data, return_counts=True)
print(np.asarray((unique, counts)).T)
print(x_data.shape)

# In[4]:


# split data into train validation and test data
print(len(x_data))
train_value = int(0.7 * len(x_data))
test_value = int(0.9 * len(x_data))

x_train = reshape(x_data[:train_value], sequence_length, params)
y_train = y_data[:train_value]

x_validation = reshape(x_data[train_value + sequence_length:test_value], sequence_length, params)
y_validation = y_data[train_value + sequence_length:test_value]

x_test_data = reshape(x_data[test_value + sequence_length:], sequence_length, params)
y_test_data = y_data[test_value + sequence_length:]

y_train = keras.utils.to_categorical(y_train, num_classes)
y_validation = keras.utils.to_categorical(y_validation, num_classes)
y_test_data = keras.utils.to_categorical(y_test_data, num_classes)

# In[ ]:


import datetime
from sklearn.utils import class_weight
from keras import optimizers


def create_model():
    print('Creating model with shape {}'.format((x_train.shape[0], (x_train.shape[1], x_train.shape[2]))))
    model = Sequential()
    model.add(Conv2D(16, kernel_size=(3, 3), padding='same', input_shape=(x_train.shape[1], x_train.shape[2], 1),
                     activation='relu'))
    model.add(Conv2D(32, kernel_size=(3, 3), padding='same', activation='relu'))
    model.add(Flatten())
    model.add(Dropout(0.5))
    model.add(Dense(64, activation='relu'))
    model.add(Dense(num_classes, activation='softmax'))
    # opt = optimizers.SGD(lr=0.001, momentum=0.9, nesterov=True)
    # opt = SGD(lr=0.0001)
    # model.compile(loss="categorical_crossentropy", optimizer=opt)
    opt = optimizers.Adam(lr=0.001)

    model.compile(loss=keras.losses.categorical_crossentropy, optimizer=opt,
                  metrics=[['categorical_accuracy', 'accuracy', 'mse']])
    print(model.summary())

    return model


def train():
    time = datetime.datetime.now().strftime("%Y%m%d-%H%M%S")
    logdir = storage_name + "logs-simple-model-new/" + time
    filepath = storage_name + "models-new/saved-model-volume-{}".format(
        sequence_length) + time + "-{epoch:02d}-{val_accuracy:.2f}.hdf5"
    tensorboard_callback = keras.callbacks.TensorBoard(log_dir=logdir)
    save_callback = keras.callbacks.ModelCheckpoint(filepath, monitor='val_loss', verbose=0, save_best_only=False,
                                                    save_weights_only=False, mode='auto', period=50)

    sample_weights = class_weight.compute_sample_weight('balanced',
                                                        y_train)
    model = create_model()
    model.fit(x_train, y_train,
              batch_size=batch_size,
              epochs=epochs,
              verbose=1,
              validation_data=(x_validation, y_validation),
              sample_weight=sample_weights,
              callbacks=[tensorboard_callback, save_callback])

    score = model.evaluate(x_validation, y_validation, verbose=0)
    model.save("forex-lstm-seq-{}.h5".format(sequence_length))
    print('Validation loss:', score[0])
    print('Validation accuracy:', score[1])

    return model


if __name__ == '__main__':
    model = train()
    result = model.predict(x_test_data)
    print(result)
