import numpy as np
from PIL import Image

# IMG_SIZE = None

def normalize_data(x_data):
    # print(  (x_data-x_data.min())/(x_data.max()-x_data.min))
    # print(x_data.min())
    # print(x_data.max())
    norm_data = x_data - x_data.min()
    diff = x_data.max() - x_data.min()
    #     print(diff)
    # print(norm_data/diff)
    return norm_data / diff


def display_as_grayscale_img(numpy_array):
    #     pixels = np.array([[0.1, 0.15], [0.2, 0.25], [0.6, 0.65]])
    #     # pixels = [0.1, 0.15, 0.2, 0.25, 0.6, 0.65]
    print(numpy_array.shape)
    pixels = 255 * (1.0 - numpy_array)
    print(pixels.T)
    #     pixels.resize(3,2)
    im = utils.Image.fromarray(pixels.T.astype(utils.np.uint8), mode='L')
    im = im.resize((pixels.T.shape[1] * 20, pixels.T.shape[0] * 50))
    im.show()


def get_y_data(x_data, n_ahead, sequence_length, pips_diff):
    y_data = []

    for i in range(0, len(x_data) - 1 - n_ahead):
        last_close = x_data[i][sequence_length - 1][3]
        y_data.append(utils.get_y_data_for_single_set(x_data, n_ahead, sequence_length, pips_diff, last_close, i))

    return y_data


def get_y_data_for_single_set(x_data, n_ahead, sequence_length, pips_diff, last_close, i):
    for fut_i in range(i + 1, i + n_ahead):
        next_max = x_data[fut_i][sequence_length - 1][1]
        next_min = x_data[fut_i][sequence_length - 1][2]
        if next_max - last_close > pips_diff:
            return 2
        elif next_min - last_close < -pips_diff:
            return 1
    return 0


def reshape(data, sequence_length, params):
    return data.reshape(data.shape[0], sequence_length, params, 1)
# # Functions
# def preprocess_observation(obs):
#     global IMG_SIZE
#     # Convert to gray-scale and resize it
#     image = Image.fromarray(obs, 'RGB').convert('L').resize(IMG_SIZE)
#     # Convert image to array and return it
#     return np.asarray(image.getdata(), dtype=np.uint8).reshape(image.size[1],
#                                                                image.size[0])
#
#
# def get_next_state(current, obs):
#     # Next state is composed by the last 3 images of the previous state and the
#     # new observation
#     return np.append(current[1:], [obs], axis=0)
